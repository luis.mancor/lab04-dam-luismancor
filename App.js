import React, {Component} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import ConexionFetch from './app/components/conexionFetch/ConexionFetch';
import DetailFetch from './app/components/conexionFetch/DetailFetch';
import Login from './app/components/conexionFetch/Login';

const Stack = createStackNavigator();
//
export default class App extends Component{
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              title: 'Login',
              headerStyle: {backgroundColor: '#AF9A6B',},
              headerTitleStyle: {
                fontStyle: 'italic',
                fontSize: 30,
              },
            }}
          />

          <Stack.Screen 
            name="Home" 
            component={ConexionFetch} 
            options={{
              title: 'Lista de Peliculas',
              headerStyle: {backgroundColor: '#AF9A6B',},
              headerTitleStyle: {
                fontStyle: 'italic',
                fontSize: 30,
              },
            }}
          />
          <Stack.Screen 
            name="Details" 
            component={DetailFetch}
            options={{
              title: 'Detalles de la Pelicula',
              headerStyle: {
                backgroundColor: '#4F3141',
              },
              headerTitleStyle: {
                fontFamily: 'bold',
                fontSize: 20,
              },
            }} 
          />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
